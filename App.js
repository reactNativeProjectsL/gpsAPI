import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Dimensions
} from 'react-native';

import MapView from 'react-native-maps';

export default class App extends Component<{}> {
  state = {
    latLng: {
      lat: 4.8133300,
      lng: -75.6961100
    },
    markers: []
  }

  componentWillMount() {
    navigator.geolocation.getCurrentPosition(
      (success) => {
        var { latitude, longitude } = success.coords;
        latLng = {lat: latitude, lng: longitude};
        this.setState({
          latLng,
          markers:[{
            title: 'Ur location',
            latLng: {latitude, longitude},
            description: 'Ur location'
          }]
        });
      }
    );

    var watchID = navigator.geolocation.watchPosition(
      (lastPosition) => {
        var { latitude, longitude } = lastPosition.coords;
        latLng = {latitude: latitude, longitude: longitude};
        this.setState({
          latLng: {lat: latitude, lng: longitude},
          markers:[{
            title: '¡Estás aquí!',
            latLng: latLng,
            description: 'Te encuentras en esta ubicación'
          }]
        });
        console.warn(`Changed pos ${latitude}-${longitude}`);
      },
      (error) => alert(JSON.stringify(error)),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 0, distanceFilter: 1}
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          region={
            new MapView.AnimatedRegion({
              latitude: this.state.latLng.lat,
              longitude: this.state.latLng.lng,
              latitudeDelta: 0.0025,
              longitudeDelta: 0.0025
            })
          }
          >
          {
            this.state.markers.map(marker => (
              <MapView.Marker
                key={marker.title}
                coordinate={marker.latLng}
                title={marker.title}
                description={marker.description}
              />
            ))
          }
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  map: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
});
